package gltf

import "core:encoding/base64"
import "core:encoding/json"
import "core:fmt"
import "core:os"
import "core:path/filepath"
import "core:image"
import "core:slice"
import "core:strconv"
import "core:strings"

@(private)
load_uri :: proc(uri: string, directory := "") -> []byte {
	data_str: string
	if strings.has_prefix(uri, "data:application/octet-stream") {
		data_str = uri[len("data:application/octet-stream") + 1:]
	} else if strings.has_prefix(uri, "data:application/gltf-buffer") {
		data_str = uri[len("data:application/gltf-buffer") + 1:]
	} else {
		return load_file(uri, directory)
	}

	to_comma := strings.index(data_str, ",")
	encoding := "base64"
	if to_comma > 0 {
		encoding = data_str[:to_comma]
		data_str = data_str[to_comma + 1:]
	}
	fmt.assertf(encoding == "base64", "Unsupported data encoding %s\n", encoding)
	
	switch encoding {
	case "base64":
		return base64.decode(data_str)
	}
	
	unreachable()
}

@(private)
load_file :: proc(file_name: string, directory := "") -> []byte {
	path := filepath.join(
		elems = {directory, file_name},
		allocator = context.temp_allocator,
	)
	raw, ok := os.read_entire_file(path)
	fmt.assertf(ok, "Failed to load file %s\n", path)
	return raw
}

// Processes a byte slice of GLTF source into a GLTF structure.
parse :: proc(source: []byte, directory := "", allocator := context.allocator) -> GLTF {
	context.allocator = allocator

	json_data, json_success := json.parse(source)
	defer json.destroy_value(json_data)

	data := json_data.(json.Object)

	result: GLTF
	out_scenes: [dynamic]Scene
	out_nodes: [dynamic]Node
	out_meshes: [dynamic]Mesh
	out_materials: [dynamic]Material
	out_skins: [dynamic]Skin
	out_images: [dynamic]^image.Image
	out_textures: [dynamic]Texture
	out_samplers: [dynamic]Texture_Sampler
	out_animations: [dynamic]Animation
	
	loaded_buffer_views: [dynamic][]byte
	loaded_accessors: [dynamic]Accessor

	defer {
		delete(loaded_buffer_views)
		delete(loaded_accessors)
	}

	// Check asset header
	{
		asset := data["asset"].(json.Object)
		fmt.assertf(asset["version"].(string) == "2.0", "Unsupported GLTF version ", asset["version"])
	}
	
	// Load buffers
	if json_buffers, has_buffers := data["buffers"]; has_buffers {
		buffers := json_buffers.(json.Array) or_else {}
		result.buffers = make([][]byte, len(buffers))
		
		for json_buffer, i in buffers {
			buffer_data := json_buffer.(json.Object) or_else {}
			
			byte_length := uint(buffer_data["byteLength"].(json.Float))
			uri := buffer_data["uri"].(string)
			
			if strings.has_prefix(uri, "data:") {
				result.buffers[i] = load_uri(uri, directory)
			} else {
				result.buffers[i] = load_file(uri, directory)
			}
		}
	}
	
	// Load buffer views
	if json_buffer_views, has_buffer_views := data["bufferViews"]; has_buffer_views {
		buffer_views := json_buffer_views.(json.Array) or_else {}
		for json_buffer_view in buffer_views {
			buffer_view_data := json_buffer_view.(json.Object) or_else {}
			
			index := uint(buffer_view_data["buffer"].(json.Float))
			length := uint(buffer_view_data["byteLength"].(json.Float))
			offset := uint(0)
			// TODO: byteStride

			if data_offset, has_offset := buffer_view_data["byteOffset"]; has_offset {
				offset = uint(data_offset.(json.Float))
			}
			
			append(&loaded_buffer_views, result.buffers[index][offset:offset + length])
		}
	}
	
	// Load buffer view accessors
	if json_accessors, has_accessors := data["accessors"]; has_accessors {
		accessors := json_accessors.(json.Array) or_else {}
		for json_accessor in accessors {
			accessor_data := json_accessor.(json.Object) or_else {}
			
			index := uint(accessor_data["bufferView"].(json.Float))
			length := uint(accessor_data["count"].(json.Float))
			offset := uint(0)
			if data_offset, has_offset := accessor_data["byteOffset"]; has_offset {
				offset = uint(data_offset.(json.Float))
			}

			type := accessor_data["type"].(string)
			component := uint(accessor_data["componentType"].(json.Float))
			
			stride := accessor_stride(type, component)
			size := stride * length

			accessor_slice := loaded_buffer_views[index][offset:offset + size]
			access: Accessor

			// HERE BE THE SWITCH OF DEATH
			switch type {
			case "SCALAR":
				switch component {
				case SBYTE: access = Accessor(slice.reinterpret([]SByte, accessor_slice))
				case UBYTE: access = Accessor(slice.reinterpret([]UByte, accessor_slice))
				case SSHORT: access = Accessor(slice.reinterpret([]SShort, accessor_slice))
				case USHORT: access = Accessor(slice.reinterpret([]UShort, accessor_slice))
				case UINT: access = Accessor(slice.reinterpret([]UInt, accessor_slice))
				case FLOAT: access = Accessor(slice.reinterpret([]Float, accessor_slice))
				}
			case "VEC2":
				switch component {
				case SBYTE: access = Accessor(slice.reinterpret([][2]SByte, accessor_slice))
				case UBYTE: access = Accessor(slice.reinterpret([][2]UByte, accessor_slice))
				case SSHORT: access = Accessor(slice.reinterpret([][2]SShort, accessor_slice))
				case USHORT: access = Accessor(slice.reinterpret([][2]UShort, accessor_slice))
				case UINT: access = Accessor(slice.reinterpret([][2]UInt, accessor_slice))
				case FLOAT: access = Accessor(slice.reinterpret([][2]Float, accessor_slice))
				}
			case "VEC3":
				switch component {
				case SBYTE: access = Accessor(slice.reinterpret([][3]SByte, accessor_slice))
				case UBYTE: access = Accessor(slice.reinterpret([][3]UByte, accessor_slice))
				case SSHORT: access = Accessor(slice.reinterpret([][3]SShort, accessor_slice))
				case USHORT: access = Accessor(slice.reinterpret([][3]UShort, accessor_slice))
				case UINT: access = Accessor(slice.reinterpret([][3]UInt, accessor_slice))
				case FLOAT: access = Accessor(slice.reinterpret([][3]Float, accessor_slice))
				}
			case "VEC4":
				switch component {
				case SBYTE: access = Accessor(slice.reinterpret([][4]SByte, accessor_slice))
				case UBYTE: access = Accessor(slice.reinterpret([][4]UByte, accessor_slice))
				case SSHORT: access = Accessor(slice.reinterpret([][4]SShort, accessor_slice))
				case USHORT: access = Accessor(slice.reinterpret([][4]UShort, accessor_slice))
				case UINT: access = Accessor(slice.reinterpret([][4]UInt, accessor_slice))
				case FLOAT: access = Accessor(slice.reinterpret([][4]Float, accessor_slice))
				}
			case "MAT2":
				switch component {
				case SBYTE: access = Accessor(slice.reinterpret([]matrix[2, 2]SByte, accessor_slice))
				case UBYTE: access = Accessor(slice.reinterpret([]matrix[2, 2]UByte, accessor_slice))
				case SSHORT: access = Accessor(slice.reinterpret([]matrix[2, 2]SShort, accessor_slice))
				case USHORT: access = Accessor(slice.reinterpret([]matrix[2, 2]UShort, accessor_slice))
				case UINT: access = Accessor(slice.reinterpret([]matrix[2, 2]UInt, accessor_slice))
				case FLOAT: access = Accessor(slice.reinterpret([]matrix[2, 2]Float, accessor_slice))
				}
			case "MAT3":
				switch component {
				case SBYTE: access = Accessor(slice.reinterpret([]matrix[3, 3]SByte, accessor_slice))
				case UBYTE: access = Accessor(slice.reinterpret([]matrix[3, 3]UByte, accessor_slice))
				case SSHORT: access = Accessor(slice.reinterpret([]matrix[3, 3]SShort, accessor_slice))
				case USHORT: access = Accessor(slice.reinterpret([]matrix[3, 3]UShort, accessor_slice))
				case UINT: access = Accessor(slice.reinterpret([]matrix[3, 3]UInt, accessor_slice))
				case FLOAT: access = Accessor(slice.reinterpret([]matrix[3, 3]Float, accessor_slice))
				}
			case "MAT4":
				switch component {
				case SBYTE: access = Accessor(slice.reinterpret([]matrix[4, 4]SByte, accessor_slice))
				case UBYTE: access = Accessor(slice.reinterpret([]matrix[4, 4]UByte, accessor_slice))
				case SSHORT: access = Accessor(slice.reinterpret([]matrix[4, 4]SShort, accessor_slice))
				case USHORT: access = Accessor(slice.reinterpret([]matrix[4, 4]UShort, accessor_slice))
				case UINT: access = Accessor(slice.reinterpret([]matrix[4, 4]UInt, accessor_slice))
				case FLOAT: access = Accessor(slice.reinterpret([]matrix[4, 4]Float, accessor_slice))
				}
			case:
				// Invalid type string
			}

			append(&loaded_accessors, access)
		}
	}

	// Load images
	if json_images, has_images := data["images"]; has_images {
		for json_image in json_images.(json.Array) {
			img := json_image.(json.Object)
			image_data: []byte
			if buf_view_index, has_buf_view := img["bufferView"]; has_buf_view {
				image_data = loaded_buffer_views[uint(buf_view_index.(json.Float))]
			} else if uri, has_uri := img["uri"]; has_uri {
				image_data = load_uri(uri.(string), directory)
			}
			loaded_img, err := image.load_from_bytes(image_data)
			if err == nil {
				append(&out_images, loaded_img)
			} else {
				fmt.eprintf("Error loading image: %s\n", err)
			}
		}
	}

	// Load samplers
	if json_samplers, has_samplers := data["samplers"]; has_samplers {
		for json_sampler in json_samplers.(json.Array) {
			sampler_data := json_sampler.(json.Object)
			sampler: Texture_Sampler

			sampler.name = strings.clone((sampler_data["name"] or_else {}).(string) or_else "")
			if mag_filter, has_mag := sampler_data["magFilter"]; has_mag {
				sampler.mag_filter = Texture_Filtering(uint(mag_filter.(json.Float)))
			}
			if min_filter, has_min := sampler_data["minFilter"]; has_min {
				sampler.min_filter = Texture_Filtering(uint(min_filter.(json.Float)))
			}
			if wrap_s, has_wrap_s := sampler_data["wrapS"]; has_wrap_s {
				sampler.wrap_s = Texture_Wrapping(uint(wrap_s.(json.Float)))
			}
			if wrap_t, has_wrap_t := sampler_data["wrapT"]; has_wrap_t {
				sampler.wrap_s = Texture_Wrapping(uint(wrap_t.(json.Float)))
			}
			append(&out_samplers, sampler)
		}
	}

	// Load textures
	if json_textures, has_textures := data["textures"]; has_textures {
		for json_texture in json_textures.(json.Array) {
			t := json_texture.(json.Object)
			texture: Texture

			texture.name = strings.clone((t["name"] or_else {}).(string) or_else "")
			if source_index, has_source := t["source"]; has_source {
				texture.source = out_images[uint(source_index.(json.Float))]
			}
			if sampler_index, has_sampler := t["sampler"]; has_sampler {
				texture.sampler = &out_samplers[uint(sampler_index.(json.Float))]
			}

			append(&out_textures, texture)
		}
	}

	// Load materials
	if json_materials, has_materials := data["materials"]; has_materials {
		materials := json_materials.(json.Array) or_else {}
		for json_material in materials {
			material: Material
			material_data := json_material.(json.Object) or_else {}
			material.name = strings.clone((material_data["name"] or_else {}).(string) or_else "")
			if json_pbr, has_pbr := material_data["pbrMetallicRoughness"]; has_pbr {
				pbr_data := json_pbr.(json.Object) or_else {}
				if json_color_factor, has_color_factor := pbr_data["baseColorFactor"]; has_color_factor {
					color_factor_data := json_color_factor.(json.Array) or_else {}
					material.pbr_metallic_roughness.base_color_factor = {
						0 = Float(color_factor_data[0].(json.Float) or_else 1.0),
						1 = Float(color_factor_data[1].(json.Float) or_else 1.0),
						2 = Float(color_factor_data[2].(json.Float) or_else 1.0),
						3 = Float(color_factor_data[3].(json.Float) or_else 1.0),
					}
				} else {
					material.pbr_metallic_roughness.base_color_factor = {1.0, 1.0, 1.0, 1.0}
				}

				if json_color_texture, has_color_texture := pbr_data["baseColorTexture"]; has_color_texture {
					color_texture_info := json_color_texture.(json.Object) or_else {}
					texture_info: Texture_Info
					if index, has_index := color_texture_info["index"]; has_index {
						texture_info.texture = &out_textures[uint(index.(json.Float))]
					}
					if index, has_index := color_texture_info["texCoord"]; has_index {
						texture_info.tex_coord = uint(index.(json.Float))
					}
					material.pbr_metallic_roughness.base_color_texture = texture_info
				}
				
				if json_metallic_texture, has_metallic_texture := pbr_data["metallicRoughnessTexture"]; has_metallic_texture {
					metallic_texture_info := json_metallic_texture.(json.Object) or_else {}
					texture_info: Texture_Info
					if index, has_index := metallic_texture_info["index"]; has_index {
						texture_info.texture = &out_textures[uint(index.(json.Float))]
					}
					if index, has_index := metallic_texture_info["texCoord"]; has_index {
						texture_info.tex_coord = uint(index.(json.Float))
					}
					material.pbr_metallic_roughness.metalic_roughness_texture = texture_info
				}

				material.pbr_metallic_roughness.metallic_factor =
					Float((pbr_data["metallicFactor"] or_else {}).(json.Float) or_else 0.0)
				material.pbr_metallic_roughness.roughness_factor =
					Float((pbr_data["roughnessFactor"] or_else {}).(json.Float) or_else 0.0)
			}

			if json_normal_texture, has_normal_texture := material_data["normalTexture"]; has_normal_texture {
				normal_texture_info := json_normal_texture.(json.Object) or_else {}			
				texture_info: Texture_Info
				if index, has_index := normal_texture_info["index"]; has_index {
					texture_info.texture = &out_textures[uint(index.(json.Float))]
				}
				if index, has_index := normal_texture_info["texCoord"]; has_index {
					texture_info.tex_coord = uint(index.(json.Float))
				}
				if index, has_index := normal_texture_info["strength"]; has_index {
					texture_info.strength = Float(index.(json.Float))
				} else {
					texture_info.strength = Float(1.0)
				}
				material.normal_texture = texture_info
			}

			if json_occ_texture, has_occ_texture := material_data["occlusionTexture"]; has_occ_texture {
				occ_texture_info := json_occ_texture.(json.Object) or_else {}
				texture_info: Texture_Info
				if index, has_index := occ_texture_info["index"]; has_index {
					texture_info.texture = &out_textures[uint(index.(json.Float))]
				}
				if index, has_index := occ_texture_info["texCoord"]; has_index {
					texture_info.tex_coord = uint(index.(json.Float))
				}
				if index, has_index := occ_texture_info["strength"]; has_index {
					texture_info.strength = Float(index.(json.Float))
				} else {
					texture_info.strength = Float(1.0)
				}
				material.occlusion_texture = texture_info
			}

			if json_emissive_texture, has_emissive_texture := material_data["emissiveTexture"]; has_emissive_texture {
				emissive_texture_info := json_emissive_texture.(json.Object) or_else {}
				texture_info: Texture_Info
				if index, has_index := emissive_texture_info["index"]; has_index {
					texture_info.texture = &out_textures[uint(index.(json.Float))]
				}
				if index, has_index := emissive_texture_info["texCoord"]; has_index {
					texture_info.tex_coord = uint(index.(json.Float))
				}
				material.emissive_texture = texture_info
			}

			if json_emissive_factor, has_emissive_factor := material_data["emissiveFactor"]; has_emissive_factor {
				emissive_factor := json_emissive_factor.(json.Array) or_else {}
				material.emissive_factor = {
					0 = Float(emissive_factor[0].(json.Float) or_else 0.0),
					1 = Float(emissive_factor[1].(json.Float) or_else 0.0),
					2 = Float(emissive_factor[2].(json.Float) or_else 0.0),
				}
			}

			if json_alpha_mode, has_alpha_mode := material_data["alphaMode"]; has_alpha_mode {
				alpha_mode := json_alpha_mode.(string)
				switch alpha_mode {
				case "OPAQUE":
					material.alpha_mode = .Opaque
				case "MASK":
					material.alpha_mode = .Mask
				case "BLEND":
					material.alpha_mode = .Blend
				case:
					material.alpha_mode = .Opaque
				}
			}

			if json_alpha_cutoff, has_alpha_cutoff := material_data["alphaCutoff"]; has_alpha_cutoff {
				material.alpha_cutoff = Float(json_alpha_cutoff.(json.Float) or_else 0.5)
			}
			if json_double_sided, has_double_sided := material_data["doubleSided"]; has_double_sided {
				material.double_sided = bool(json_double_sided.(json.Boolean) or_else false)
			}
			append(&out_materials, material)
		}
	}
	
	// Load meshes
	if json_meshes, has_meshes := data["meshes"]; has_meshes {
		meshes := json_meshes.(json.Array) or_else {}
		for json_mesh in meshes {
			mesh: Mesh
			mesh_data := json_mesh.(json.Object) or_else {}
			mesh.name = strings.clone((mesh_data["name"] or_else {}).(string) or_else "")
			
			if json_prims, has_prims := mesh_data["primitives"]; has_prims {
				prims := json_prims.(json.Array) or_else {}
				mesh.primitives = make([]Primitive, len(prims))
				for json_prim, i in prims {
					prim := json_prim.(json.Object) or_else {}
					
					if json_indices, has_indices := prim["indices"]; has_indices {
						mesh.primitives[i].indices = loaded_accessors[uint(json_indices.(json.Float))]
					}

					material_index := uint((prim["material"] or_else {}).(json.Float) or_else 0.0)
					if material_index < len(out_materials) {
						mesh.primitives[i].material = &out_materials[material_index]
						mesh.primitives[i].material_index = material_index
					}
					
					tex_coords: [dynamic]Accessor
					color: [dynamic]Accessor
					joints: [dynamic]Accessor
					weights: [dynamic]Accessor

					if json_attrs, has_attrs := prim["attributes"]; has_attrs {
						attrs := json_attrs.(json.Object) or_else {}
						for attr_name, json_accessor_index in attrs {
							accessor_index := uint(json_accessor_index.(json.Float))
							
							switch attr_name {
							case "POSITION":
								mesh.primitives[i].position = loaded_accessors[accessor_index].([][3]Float)
							case "NORMAL":
								mesh.primitives[i].normal = loaded_accessors[accessor_index].([][3]Float)
							case "TANGENT":
								mesh.primitives[i].tangent = loaded_accessors[accessor_index].([][4]Float)
							case:
								// skip application-specific attributes
								if attr_name[0] == ' ' { continue }
								
								underscore := strings.index(attr_name, "_")
								name := attr_name[:underscore]
								index, index_ok := strconv.parse_uint(attr_name[underscore + 1:])
								
								target_array: ^[dynamic]Accessor
								switch name {
								case "TEXCOORD":
									target_array = &tex_coords
								case "COLOR":
									target_array = &color
								case "JOINTS":
									target_array = &joints
								case "WEIGHTS":
									target_array = &weights
								}
								
								if index >= len(target_array^) { resize(target_array, int(index + 1)) }
								target_array[index] = loaded_accessors[accessor_index]
							}
						}
					}
					
					shrink(&tex_coords)
					shrink(&color)
					shrink(&joints)
					shrink(&weights)
					mesh.primitives[i].tex_coord = tex_coords[:]
					mesh.primitives[i].color = color[:]
					mesh.primitives[i].joints = joints[:]
					mesh.primitives[i].weights = weights[:]
				}
			}
			
			append(&out_meshes, mesh)
		}
	}

	// Load nodes
	if json_nodes, has_nodes := data["nodes"]; has_nodes {
		nodes := json_nodes.(json.Array) or_else {}
		for json_node in nodes {
			node: Node
			node_data := json_node.(json.Object) or_else {}
			
			node.name = strings.clone((node_data["name"] or_else {}).(string) or_else "")
			
			if mesh_json, has_mesh := node_data["mesh"]; has_mesh {
				node.mesh = &out_meshes[uint(mesh_json.(json.Float))]
			}
			
			if trans_json, has_trans := node_data["translation"]; has_trans {
				trans := trans_json.(json.Array)
				node.translation.x = Float(trans[0].(json.Float))
				node.translation.y = Float(trans[1].(json.Float))
				node.translation.z = Float(trans[2].(json.Float))
			}
			if rot_json, has_rot := node_data["rotation"]; has_rot {
				rot := rot_json.(json.Array)
				node.rotation.x = Float(rot[0].(json.Float))
				node.rotation.y = Float(rot[1].(json.Float))
				node.rotation.z = Float(rot[2].(json.Float))
				node.rotation.w = Float(rot[3].(json.Float))
			} else {
				node.rotation.w = 1.0
			}
			if scale_json, has_scale := node_data["scale"]; has_scale {
				scale := scale_json.(json.Array)
				node.scale.x = Float(scale[0].(json.Float))
				node.scale.y = Float(scale[1].(json.Float))
				node.scale.z = Float(scale[2].(json.Float))
			} else {
				node.scale = {1.0, 1.0, 1.0}
			}
			
			append(&out_nodes, node)
		}
			
		// Iterate back through to set children
		// We do so after initializing all nodes so the children exist in the first
		// place
		for n := 0; n < len(out_nodes); n += 1 {
			node_data := nodes[n].(json.Object)
			if children_json, has_children := node_data["children"]; has_children {
				children := children_json.(json.Array)
				node_children: [dynamic]^Node
				for child_index in children {
					append(&node_children, &out_nodes[uint(child_index.(json.Float))])
				}
				out_nodes[n].children = node_children[:]
			}
		}
	}
	
	// Load animations
	if json_animations, has_animations := data["animations"]; has_animations {
		animations_data := json_animations.(json.Array)
		for json_animation, i in animations_data {
			animation_data := json_animation.(json.Object)
			animation: Animation

			animation.name = strings.clone((animation_data["name"] or_else {}).(string) or_else "")

			samplers_data := animation_data["samplers"].(json.Array)
			animation.samplers = make([]Animation_Sampler, len(samplers_data))
			
			for json_sampler, i in samplers_data {
				sampler_data := json_sampler.(json.Object)

				interpolation := sampler_data["interpolation"].(string)
				input_index := uint(sampler_data["input"].(json.Float))
				output_index := uint(sampler_data["output"].(json.Float))
				
				animation.samplers[i].interpolation =
					interpolation == "LINEAR" ? .Linear :
					interpolation == "STEP" ? .Step :
					.Cubic_Spline
				animation.samplers[i].input = loaded_accessors[input_index].([]Float)
				animation.samplers[i].output = loaded_accessors[output_index]
			}
			
			channels_data := animation_data["channels"].(json.Array)
			animation.channels = make([]Animation_Channel, len(channels_data))

			for json_channel, i in channels_data {
				channel_data := json_channel.(json.Object)
				target_data := channel_data["target"].(json.Object)
				target_path := target_data["path"].(string)
				
				animation.channels[i].sampler = &animation.samplers[uint(channel_data["sampler"].(json.Float))]
				animation.channels[i].target.node = &out_nodes[uint(target_data["node"].(json.Float))]
				animation.channels[i].target.path =
					target_path == "rotation" ? .Rotation :
					target_path == "translation" ? .Translation :
					target_path == "scale" ? .Scale :
					.Weights
			}
			
			append(&out_animations, animation)
		}
	}

	// Load skins
	if json_skins, has_skins := data["skins"]; has_skins {
		skins := json_skins.(json.Array) or_else {}
		for json_skin in skins {
			skin: Skin
			skin_data := json_skin.(json.Object)

			joints: [dynamic]^Node
			
			for json_joint in skin_data["joints"].(json.Array) {
				append(&joints, &out_nodes[uint(json_joint.(json.Float))])
			}
			if json_skeleton, has_skeleton := skin_data["skeleton"]; has_skeleton {
				skin.skeleton = &out_nodes[uint(json_skeleton.(json.Float))]
			}
			
			if json_mats, has_mats := skin_data["inverseBindMatrices"]; has_mats {
				skin.inverse_bind_matrices = loaded_accessors[uint(json_mats.(json.Float))].([]matrix[4, 4]Float)
			}
			
			skin.joints = joints[:]
			append(&out_skins, skin)
		}
	}

	// Load scenes
	if json_scenes, has_scenes := data["scenes"]; has_scenes {
		scenes := json_scenes.(json.Array) or_else {}
		for json_scene in scenes {
			scene: Scene
			scene_data := json_scene.(json.Object) or_else {}

			scene.name = strings.clone(scene_data["name"].(string) or_else "")
			if json_nodes, has_nodes := scene_data["nodes"]; has_nodes {
				nodes := json_nodes.(json.Array)
				scene_nodes: [dynamic]^Node
				for node_index in nodes {
					append(&scene_nodes, &out_nodes[uint(node_index.(json.Float))])
				}
				scene.nodes = scene_nodes[:]
			}

			append(&out_scenes, scene)
		}
	}

	// Load root scene
	if json_scene, has_scene := data["scene"]; has_scene {
		scene_index := json_scene.(json.Float) or_else 0
		result.root_scene = &out_scenes[uint(scene_index)]
	}

	result.scenes = out_scenes[:]
	result.nodes = out_nodes[:]
	result.materials = out_materials[:]
	result.meshes = out_meshes[:]
	result.animations = out_animations[:]
	result.skins = out_skins[:]
	result.images = out_images[:]
	result.textures = out_textures[:]
	result.samplers = out_samplers[:]

	return result
}

// Destroys a loaded GLTF data structure.
destroy :: proc(data: GLTF) {
	if len(data.scenes) > 0 {
		for scene in data.scenes {
			delete(scene.name)
			delete(scene.nodes)
		}
		delete(data.scenes)
	}
	if len(data.nodes) > 0 {
		for node in data.nodes {
			delete(node.name)
			delete(node.children)
		}
		delete(data.nodes)
	}
	if len(data.materials) > 0 {
		delete(data.materials)
	}
	if len(data.animations) > 0 {
		for animation in data.animations {
			delete(animation.name)
			delete(animation.channels)
			delete(animation.samplers)
		}
		delete(data.animations)
	}
	if len(data.meshes) > 0 {
		for mesh in data.meshes {
			for primitive in mesh.primitives {
				delete(primitive.tex_coord)
				delete(primitive.color)
				delete(primitive.joints)
				delete(primitive.weights)
			}
			delete(mesh.name)
			delete(mesh.primitives)
		}
		delete(data.meshes)
	}
	if len(data.skins) > 0 {
		for skin in data.skins {
			if len(skin.joints) > 0 { delete(skin.joints) }
			// Unnecessary: slice into buffers, which we delete momentarily
			// if len(skin.inverse_bind_matrices) > 0 { delete(skin.inverse_bind_matrices) }
		}
		delete(data.skins)
	}
	if len(data.images) > 0 {
		for img in data.images {
			image.destroy(img)
		}
		delete(data.images)
	}
	if len(data.samplers) > 0 {
		for sampler in data.samplers {
			delete(sampler.name)
		}
		delete(data.samplers)
	}
	if len(data.textures) > 0 {
		for texture in data.textures {
			delete(texture.name)
		}
		delete(data.textures)
	}
	if len(data.buffers) > 0 {
		for buffer in data.buffers {
			delete(buffer)
		}
		delete(data.buffers)
	}
}
